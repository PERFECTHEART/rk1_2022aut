#include "solve.h"

#include <cassert>
#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		std::cerr << "Incorrect number of args, expected 4" << std::endl;
		return 1;
	}

	double a = 0, b = 0, c = 0;
	try
	{
		a = std::stod(argv[1]);
		b = std::stod(argv[2]);
		c = std::stod(argv[3]);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	std::cout << "Solving: " << a << "*x*x + " << b << "*x + " << c << " = 0" << std::endl;

	Solution s = Solve(a, b, c);
	switch (s.type)
	{
	case Solution::SolutionType::INFINITE_ROOTS:
		std::cout << "Any x is solution" << std::endl;

	case Solution::SolutionType::NO_ROOTS:
		std::cout << "No roots" << std::endl;
		break;

	case Solution::SolutionType::ONE_ROOT:
		assert(s.x1 == s.x2);
		std::cout << "One root x = " << s.x1 << std::endl;
		break;

	case Solution::SolutionType::TWO_ROOTS:
		std::cout << "x1 = " << s.x1 << "; x2 = " << s.x2 << std::endl;
		break;

	default:
		std::abort();
	}
}
