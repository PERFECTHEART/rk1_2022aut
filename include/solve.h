#pragma once

#include <cassert>

struct Solution
{
public:
    enum class SolutionType
    {
        TWO_ROOTS,
        ONE_ROOT, // If ONE_ROOT => x1 == x2
        NO_ROOTS, // x1 == x2 == 0. (default)
        INFINITE_ROOTS // -- || --
    };

public:
    Solution(SolutionType t)
        : type(t)
    {
        assert(t == SolutionType::NO_ROOTS || t == SolutionType::INFINITE_ROOTS);
    }

    Solution(double x)
        : type(SolutionType::ONE_ROOT)
        , x1(x)
        , x2(x)
    {}

    Solution(double x1, double x2)
        : type(SolutionType::TWO_ROOTS)
        , x1(x1)
        , x2(x2)
    {}

public:
    SolutionType type;

    double x1 = 0.;
    double x2 = 0.;
};

Solution Solve(double a, double b, double c);

